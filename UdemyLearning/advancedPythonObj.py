
# convert to binary and hex
a = bin(1024)
print("1024 in Binary : ", a)
b = hex(1024)
print("1024 in Hexadecimal : ", b)

# round to 2 decimal places
c = round(5.23222, 2)
print("Round of 5.23222 : ", c)

# string check
s = 'hello how are you Mary, are you feeling okay?'
print("Is all lower : ", s.islower())

# count letter
s = 'twywywtwywbwhsjhwuwshshwuwwwjdjdid'
count = s.count('w')
print("Count of letter 'w' is : {}".format(count))

# difference in set
set1 = {2, 3, 1, 5, 6, 8}
set2 = {3, 1, 7, 5, 6, 8}
print("Difference between set1 and set2 is : ", set1.difference(set2))

# similarity in set
set1 = {2, 3, 1, 5, 6, 8}
set2 = {3, 1, 7, 5, 6, 8}
print(f"similar set in two sets : {set1.intersection(set2)}")

# Either in all set, Union of two  set
set1 = {2, 3, 1, 5, 6, 8}
set2 = {3, 1, 7, 5, 6, 8}
print(f"Union in two sets : {set1.union(set2)}")

# dictionary comprehension
# {0: 0, 1: 1, 2: 8, 3: 27, 4: 64}
d = {x: x**3 for x in range(5)}
print("Comprehension Dict : ", d)

# list comprehension
l = [x**3 for x in range(5)]
print("Comprehension List : ", l)

# reverse list
list1 = [1, 2, 3, 4]
list1.reverse()
print("Reversed list : ", list1)

list2 = [3, 4, 2, 5, 1]
list2.sort()
print("Sorted list : ", list2)
















