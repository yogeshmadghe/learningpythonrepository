print("Hello World !!")

#String
a = "I am a string"
print(a)
print("Another way to print a is :", a)

#List
list1 = [1,2,3,5,'A','B','C']
print(list1)
print(f'the length of list is {len(list1)}')

#tuple
tup = (1,2,3,4,'A',1,2,3)
print(tup)

#dictionary
dict = { 'a': 1, 'b' : 2}
print("a + b = {}".format(dict['a'] + dict['b']))

#set
set1 = {1,2,3,1,2,3,'a','b','c','a','b','c'}
print(set1)

#method (ex : len)
print("the length of set is {}".format(len(set1)))

#Function
def sayHello(string):
    print(string)

sayHello("Hello World !!")

#range method
list2 = []
for num in range(1,100):
    list2.append(num)

print(list2)
#print("Array from 1 to 100 : " + list2)

print(type(set1))