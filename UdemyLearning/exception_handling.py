'''
This is a Module docstring
'''

def ask():
    '''
    This is a function
    :return: integer
    '''
    while True:

        try:
            number = int(input("Please enter input here : "))
            print(number ** 2)

        except TypeError:
            print("Please enter an interger ")
            continue

        else:
            print("Thanks for entering number !")
            break


ask()
