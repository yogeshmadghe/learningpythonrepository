'''
I am in unittest_learning module
'''

def capital(text):
    '''
    I am a capital function
    :param text: string
    :return: string
    '''
    text = text.title()
    return text

CAP = capital("monty python's flying circus")
print(CAP)
