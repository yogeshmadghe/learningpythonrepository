'''
UNITTEST Moodule
'''
import unittest
import unittest_learning

class TestFunction(unittest.TestCase):
    '''
    This is a TestFunction, inherits from unittest packages and TestClass module
    '''

    def test_capital_one_word(self):
        '''
        check for one string
        :return: string
        '''
        text = "python"
        result = unittest_learning.capital(text)
        self.assertEqual(result, "Python")

    def test_capital_multi_word(self):
        '''
        check for multiple words
        :return: string
        '''
        text = "python book"
        result = unittest_learning.capital(text)
        self.assertEqual(result, "Python Book")

    def test_with_apostrophes(self):
        '''
        check for words having apostrophe
        :return: string
        '''
        text = "monty python's flying circus"
        result = unittest_learning.capital(text)
        self.assertEqual(result, "Monty Python's Flying Circus")


if __name__ == "__main__":
    unittest.main()
