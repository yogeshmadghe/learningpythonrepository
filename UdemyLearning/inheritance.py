
class Animal():

    def __init__(self):
        print("I am an Animal")

    def whoiam(self):
        print("I am loyal")

    def eat(self):
        print("eat meat")

class Dog(Animal):

    def __init__(self, name):
        Animal.__init__(self)
        self.name = name
        print("I am a Dog")

    def speak(self):
        print(self.name + " Bark")

class Cat():

    def __init__(self, name):
        self.name = name
        print("I am a Cat")

    def speak(self):
        print(self.name + " Miao")


d = Dog("Moti")
c = Cat("Mani")

for pet in (d,c):
    pet.speak()

d.speak()
c.speak()