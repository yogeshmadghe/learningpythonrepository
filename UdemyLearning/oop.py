
class Circle():

    pi = 3.14;
    diameter = 5;

    def __init__(self, radius = 2, diameter = 2):
        self.radius = radius
        self.area = radius * radius * self.pi
        self.diameter = diameter

    def getArea(self):
        return self.area;

    def getCircumference(self):
        return 2 * self.pi * self.radius

    def getDiameter(self):
        return self.diameter

my_circle = Circle(5,10)
print(type(my_circle))
print( f'Formula of area of circle is : {my_circle.pi} * {my_circle.radius} * {my_circle.radius} = {my_circle.getArea()} ')
print(" Formula for Circumference of circle is : 2 * pi * r => {}".format(my_circle.getCircumference()))
print("Diameter of circle is = " + str(my_circle.getDiameter()))