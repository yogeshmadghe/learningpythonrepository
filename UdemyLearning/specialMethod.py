class Book():
    def __init__(self, title, author, pages):
        self.name = title
        self.author = author
        self.pages = pages

    def __str__(self):
        return (f"I am a book of {self.name}, written by {self.author} of pages {self.pages}")

    def __len__(self):
        return self.pages

    def __del__(self):
        print("A book object has been deleted")

b = Book("Python","Yogesh",300)
print( f"book Details are => title : {b.name} , author : {b.author}, pages : {b.pages} ")

#type(b)
print(str(b))
print(len(b))

del b
