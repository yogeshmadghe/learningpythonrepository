'''
Collection module learning
'''

from collections import Counter

# with list
list1 = [1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 3, 3, 3, 3, 3, 44, 4, 4, 5, 5, 6, 67, 7, 7, 8, 8, 8]
print(Counter(list1))

# with string
s = 'aaavvvfffvvvccggggfdfjrlkhalhskkkkfjaxldaofeingciwefivwqinwqicgwqemgciqwguqgfomeiwgevqfff'
print(Counter(s))

# with sentences
sen = "I am Yogesh and I am a great guy and I am a boy and I am very honest"
words = sen.split()
print(words)
print(Counter(words))


# methods with Counter
c = Counter(words)
print(type(c))
print("I am c : ", c)

result = c.most_common(2)
print(result)
print(c.most_common()[:2-1:-1])

print(c.values())
print(c.keys())  # returns class
print(list(c))  # returns list of unique
print(set(c))  # returns set
print(dict(c))  # returns dict
print(c.items())
print(Counter(dict(c.items())))

c += Counter()
print("last c ", c)

c.clear()
print(c)

try:
    tup = [1, 2, 'yog', 'sum', 'zah', 'mit', 'jay', 'abh', 9, 546, 876, 9, 6, 6]
    print(tup)
    for i in range(len(tup)):
        tup[i] = 'you'

    print(tup)
except:
    print("There is something wrong here")

from collections import namedtuple
Animal = namedtuple('animal', 'legs breed height type name')
Tiger = Animal(legs=4, breed='cat', height=4, type='ground', name='Sher Khan')
print(Tiger.legs, Tiger.breed)






