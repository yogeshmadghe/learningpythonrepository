'''
GENERATER LEARNING
'''
#problem 1

import random


def get_square(number):
    '''
    example 1
    :param number: list
    :return: generaters
    '''
    for i in range(number):
        yield i**2

print(get_square(10))
for x in get_square(10):
    print(x)

print("========2=============")
#problem 2
print(random.randint(1, 10))

def get_random(low, high, number):
    '''
    Example 2
    :param low: int
    :param high: int
    :param number: int
    :return: generaters list
    '''
    for i in range(number):
        yield random.randint(low, high)

print(get_random(1, 10, 12))
for x in get_random(1, 10, 12):
    print(x)

print("=======3=============")
#problem 3

S = 'hello'
#for char in s:
    #print(char)
ITES_S = iter(S)
print(next(ITES_S))
print(next(ITES_S))
print(next(ITES_S))

print("========4===========")
#problem 4

LIST = [1, 2, 3, 4, 5]
GENCOMP = (item for item in LIST if item > 3)
print(GENCOMP)
for item in GENCOMP:
    print(item)
