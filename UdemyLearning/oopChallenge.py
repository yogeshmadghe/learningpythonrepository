
class Account():

    def __init__(self, owner, balance):
        self.owner = owner
        self.balance = balance

    def deposite(self, amnt):
        self.balance = self.balance + amnt
        print("Deposited {} just now into your account".format(amnt))

    def withdraw(self, amnt):
        if(self.balance < amnt):
            print("Funds Unavailable !")
        else:
            self.balance = self.balance - amnt
            print("Withdrawal Accepted for amount {}".format(amnt))

    def __str__(self):
        info = "Account Owner: {}".format(self.owner)
        info += "\nAccount Balance: " + str(self.balance)
        return info


acct = Account('Jose',100)
print(acct)
#print(acct.balance)
print(acct.owner)
print(acct.balance)
acct.deposite(50)
print("New Balance :", acct.balance)
acct.withdraw(75)
print("New Balance :", acct.balance)
acct.withdraw(76)

